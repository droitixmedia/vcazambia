@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

   <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Admin</h1>
                    <ul>
                        <li><a href="href.html">Create Coins</a></li>

                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>
                <div class="row">
                    <div class="col-lg-6 col-md-12">

                        <div class="card mb-5">
                            <div class="card-body">
                                <div class="row row-xs">

                                       <form action="{{ route('listings.store', [$area]) }}" method="post">
                                        <div class=" col-lg-12">
                              @include('listings.partials.forms._categories')
                                        </div>

                                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                            <label>Amount</label>
                                            <input type="text" name="amount" class="form-control">

                                              @if ($errors->has('amount'))
                                <span class="help-block">
                                    {{ $errors->first('amount') }}
                                </span>
                            @endif
                                        </div>
                                        <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">

                                            <button type="submit" class="btn btn-primary">Create</button>

                                         {{ csrf_field() }}
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
