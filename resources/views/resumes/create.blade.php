@extends('layouts.userapp')
@section('title')
   Create Your Resume  | Openjobs360
@endsection
@section('content')
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{ Auth::user()->fullname }}</h5>
                                <p>{{ Auth::user()->province }}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                          <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li class="active"><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                            <li><a href="#"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="#"><span class="flaticon-analysis"></span> CV Manager</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('resumes.store', [$area]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Lets craft your resume for recruiters and hiring companies</h4>
                            </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add Part of Your Education</h4>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group{{ $errors->has('institution1') ? ' has-error' : '' }}">
                                    <label for="formGroupExampleInput1">Academic Institution</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="UNISA" name="institution1">

                                     @if ($errors->has('institution1'))
                                <span class="help-block">
                                    {{ $errors->first('institution1') }}
                                </span>
                            @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">Name of Qualification</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="BEng Industrial Engineering " name="edutitle1">
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Education Level</label><br>
                                    <select name="edulevel1" class="selectpicker">

                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option selected="selected" value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="edufromyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart')
                                 </select>
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Finished</label><br>
                        <select name="edutoyear1" id="position" class="selectpicker">
                             @include('resumes.partials.forms._yearfinish')
                               </select>
                                </div>
                            </div>


                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add your work history</h4>
                            </div>

                           <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Company Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="comp1" placeholder="Droitix Robotics">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">City/Town</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2" name="comp1loc" placeholder="Cape Town">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="comp1title" placeholder="Lead Developer">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput3">Employment Type</label><br>
                                         <select name="comp1type" id="position" class="selectpicker">
                                        <option value="Full-Time">Full-Time</option>
                                        <option value="Part-Time">Part-Time</option>
                                        <option value="Temporary">Temporary</option>
                                        <option value="Contract">Contract</option>
                                        <option value="Internship">Internship</option>
                                        <option value="Remote">Remote Work</option>
                                        <option value="Freelance">Freelance</option>
                                        <option value="Commision">Commision</option>
                                        <option value="Volunteer">Volunteer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                               @include('listings.partials.forms._categories')
                            </div>

                            <div class="col-md-6 col-lg-6">
                                <div class="my_resume_textarea">
                                     <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Job Description</label>
                                        <textarea class="form-control" name="comp1about" id="jobdescrip" rows="3">
                                        </textarea>

                                      </div>
                                </div>
                            </div>
                           <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="comp1fromyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart')
                                 </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Finished</label><br>
                         <select name="comp1toyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearfinish')
                                 </select>
                                </div>
                            </div>
                             <div class="col-lg-12">
                                <h4 class="fz20 mb20">Tell us what you expect on your next job</h4>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="formGroupExampleInput1">Position Type</label><br>
                                <select name="nxtjobtype" id="position" class="selectpicker">
                                        <option value="Full-Time">Full-Time</option>
                                        <option value="Part-Time">Part-Time</option>
                                        <option value="Temporary">Temporary</option>
                                        <option value="Contract">Contract</option>
                                        <option value="Internship">Internship</option>
                                        <option value="Remote">Remote Work</option>
                                        <option value="Freelance">Freelance</option>
                                        <option value="Commision">Commision</option>
                                        <option value="Volunteer">Volunteer</option>
                                    </select>
                                </div>
                            </div>



                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput3">Willing to relocate</label><br>
                                         <select name="nxjobrelocate" id="position" class="selectpicker">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput2">Job Title</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput2" name="nxtjobtitle" placeholder="Lead Developer">
                                </div>
                            </div>
                             <input type="hidden" name="area_id" value="50">


                            <div class="col-lg-4">
                                <div class="my_profile_input">
                                    <button class="btn btn-lg btn-thm" type="submit">Submit</button>
                                </div>
                            </div>

                             {{ csrf_field() }}
                        </form><!-- form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
