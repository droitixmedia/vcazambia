<tr class="mb30">
                                <th scope="row">
                                                    <ul>
                                                        <li class="list-inline-item"><a href=""><i class="fe fe-document"></i></a></li>
                                                        <li class="list-inline-item cv_sbtitle"><a href="{{ asset('storage/'.$file->filename) }}">Proof of Payment for {{ $file->user->name }} {{ $file->user->surname }}</a>

                                        </li>
                                                    </ul>
                                                </th>
                                                <td>{{$file->created_at}}</td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <ul class="view_edit_delete_list">
                                                         <li class="list-inline-item"><a href="{{ asset('storage/'.$file->filename) }}" data-toggle="tooltip" data-placement="top" title="Download"><span class="flaticon-download"></span></a></li>

                                                        <li class="list-inline-item"><a href="#" onclick="event.preventDefault(); document.getElementById('files-destroy-form-{{ $file->id }}').submit();" data-toggle="tooltip" data-placement="bottom" title="Delete Document"><span class="flaticon-rubbish-bin"></span></a></li>
                                                    </ul>
                                                </td>
                                            </tr>
 <form action="{{ route('files.destroy', [$file]) }}" method="post" id="files-destroy-form-{{ $file->id }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
