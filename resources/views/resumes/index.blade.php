@extends('layouts.regapp')

@section('content')

<!-- Our Candidate List -->
    <section class="our-faq bgc-fa mt50">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xl-3 dn-smd">
                    <div class="faq_search_widget mb30">
                        <h4 class="fz20 mb15">Search Keywords</h4>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Find Your Question" aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2"><span class="flaticon-search"></span></button>
                            </div>
                        </div>
                    </div>


                    <div class="cl_latest_activity mb30">
                        <h4 class="fz20 mb15">Date Posted</h4>
                        <div class="ui_kit_radiobox">
                            <div class="radio">
                                <input id="radio_one"  name="radio" type="radio" checked>
                                <label for="radio_one"><span class="radio-label"></span> Latest</label>
                            </div>

                        </div>
                    </div>
                    <div class="cl_latest_activity mb30">
                        <h4 class="fz20 mb15">Job Type</h4>
                        <div class="ui_kit_whitchbox">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input" id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">All</label>
                            </div>
                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch6">
                                                <label class="custom-control-label" for="customSwitch6">Freelance</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch7">
                                                <label class="custom-control-label" for="customSwitch7">Full Time</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch8">
                                                <label class="custom-control-label" for="customSwitch8">Part Time</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch9">
                                                <label class="custom-control-label" for="customSwitch9">Internship</label>
                                            </div>
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch10">
                                                <label class="custom-control-label" for="customSwitch10">Temporary</label>
                                            </div>

                        </div>
                    </div>



                </div>
                <div class="col-lg-9 col-xl-9">
                    <div class="row">
                        <div class="col-lg-12 mb30">
                            <h4 class="fz20 mb15">{{ $category->listings->count() }} South African {{$category->name}} Jobs</h4>
                            <div class="tags-bar">
                                 <a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a>
                                 <a href="#"><i class="fa fa-arrow-right"></i> Jobs in {{$category->name}}</a>

                                <div class="action-tags">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-12 col-lg-6">

                            <div class="content_details">
                                <div class="details">

                                    <div class="faq_search_widget mb30">
                                        <h4 class="fz20 mb15">Search Keywords</h4>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Find Your Question" aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="button-addon4"><span class="flaticon-search"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ($listings->count())
                  @foreach ($listings as $listing)
            @include ('listings.partials.listing', compact('listing'))
                      @endforeach


                   {{ $listings->links() }}
              @else
                 <p>No Jobs listed in {{$category->name}} at the moment.</p>
                     @endif


                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
