@extends('layouts.userapp')
@section('title')
My Viewed Jobs  | Openjobs360
@endsection
@section('content')
@if(Auth::check())
        @if (Auth::user()->truecompany())
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{$user->companyname}}</h5>
                                <p>{{$user->province}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                        <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Company Profile</a></li>
                            <li><a href="{{ route('listings.create', [$area]) }}"><span class="flaticon-resume"></span> Post a New Job</a></li>
                            <li ><a href="{{ route('listings.published.index', [$area]) }}"><span class="flaticon-paper-plane"></span> Manage Live Jobs</a></li>
                            <li ><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-analysis"></span> Shortlisted Resumes</a></li>
                            <li class="active"><a  href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>


                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('listings.store', [$area]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Showing your last {{ $indexLimit }} viewed jobs.</h4>
                            </div>



                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>


                          <div class="col-lg-12">
                            <div class="cnddte_fvrt_job candidate_job_reivew">
                                <div class="table-responsive job_review_table">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Job Title</th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                                <th scope="col">Date Advertised</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>


    @if ($listings->count())
        @each ('listings.partials._listing_viewed', $listings, 'listing')
    @else
        <p>You have not viewed any jobs yet!</p>
    @endif

                                        </tbody>
                                    </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@else

<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{$user->fullname}}</h5>
                                <p>{{$user->province}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                           <ul>

                             <li ><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li  ><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                             <li><a href="{{ route('comments.published.index') }}"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li ><a href="{{route('files.published.index')}}"><span class="flaticon-doc"></span> Manage CV uploads</a></li>
                            <li><a href="{{route('files.upload.index')}}"><span class="flaticon-upload"></span> Upload Documents</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li class="active" ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('listings.store', [$area]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Showing your last {{ $indexLimit }} viewed jobs.</h4>
                            </div>


                        </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>


                          <div class="col-lg-12">
                            <div class="cnddte_fvrt_job candidate_job_reivew">
                                <div class="table-responsive job_review_table">
                                    <table class="table">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Job Title</th>
                                                <th scope="col"></th>
                                                <th scope="col"></th>
                                                <th scope="col">Date Advertised</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>


    @if ($listings->count())
        @each ('listings.partials._listing_viewed', $listings, 'listing')
    @else
        <th>  <p style="text-align: center;">You have not viewed any jobs yet</p></th>
    @endif

                                        </tbody>
                                    </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endif
@endif

@endsection
