@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Place Bid</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>


                                                                        @if ($listings->count())
        @each ('listings.partials.listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">No Bids yet</p></th>
    @endif
                 </div>


                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
