<div class="side-content-wrap">
            <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
                <ul class="navigation-left">

                    @role('admin')
                     <li class="nav-item"><a class="nav-item-hold" href="{{ route('listings.create', [$area]) }}"><i class="nav-icon i-Lock"></i><span class="nav-text">Create Bid</span></a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item"><a class="nav-item-hold" href="{{ url('admin/impersonate')}}"><i class="nav-icon i-Lock"></i><span class="nav-text">Impersonate</span></a>
                        <div class="triangle"></div>
                    </li>

                    @endrole
                    @if (session()->has('impersonate'))
                     <li class="nav-item"><a class="nav-item-hold" href="{{ route('listings.create', [$area]) }}"><i class="nav-icon i-Lock"></i><span class="nav-text">Create Bid</span></a>
                        <div class="triangle"></div>
                    </li>

                        <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="bx bx-lock"></i><span>Stop Impersonating</span></a>
                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

                    @endif

                    <li class="nav-item"><a class="nav-item-hold" href="{{url('/')}}"><i class="nav-icon i-Suitcase"></i><span class="nav-text">Dash Panel</span></a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item"><a class="nav-item-hold" href="{{url('bidding')}}"><i class="nav-icon i-Library"></i><span class="nav-text">BiddingRoom</span></a>
                        <div class="triangle"></div>
                    </li>
                     <li class="nav-item"><a class="nav-item-hold" href="{{ route('comments.published.index') }}"><i class="nav-icon i-Computer-Secure"></i><span class="nav-text">My Bids</span></a>
                        <div class="triangle"></div>
                    </li>
                    <li class="nav-item"><a class="nav-item-hold" href="{{ route('listings.unpublished.index', [$area]) }}"><i class="nav-icon i-Safe-Box1"></i><span class="nav-text">Maturing Coins</span></a>
                        <div class="triangle"></div>
                    </li>
                     <li class="nav-item"><a class="nav-item-hold" href="{{ route('listings.published.index', [$area]) }}"><i class="nav-icon i-Double-Tap"></i><span class="nav-text">My Selling Coins</span></a>
                        <div class="triangle"></div>
                    </li>

                     <li class="nav-item"><a class="nav-item-hold" href="{{ route('profile') }}"><i class="nav-icon i-Administrator"></i><span class="nav-text">Profile</span></a>
                        <div class="triangle"></div>
                    </li>

                </ul>
            </div>

            <div class="sidebar-overlay"></div>
        </div>
        <!-- =============== Left side End ================-->
