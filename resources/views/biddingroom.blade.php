@extends('layouts.app')

@section('title', 'Bidding Room')

@section('description')

@endsection

@section('content')

@include('layouts.partials.sidebar')

@widget('LiveBids')

@endsection
