@extends('layouts.app')
@section('title', 'Change Password')

@section('description')

@endsection

@section('content')

<!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
                    <div id="sidebar-menu" class="sidebar-menu">
                        <ul>

                            <li class="active">
                                <a href="{{url('/')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                            </li>
                            @role('admin')
                            <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                              <li>
                                <a href="{{ url('admin/impersonate')}}"><i class="fe fe-lock"></i> <span>Impersonate</span></a>
                            </li>
                            @endrole
                            @if (session()->has('impersonate'))
                              <li>
                                <a href="{{ route('listings.create', [$area]) }}"><i class="fe fe-lock"></i> <span>Create Bid</span></a>
                            </li>
                            @endif
                            @if (session()->has('impersonate'))
                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('impersonating').submit();"><i class="fe fe-lock"></i> Stop Impersonating</a>
                        </li>
                        <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>

              @endif

                            <li>
                                <a href="{{url('bidding')}}"><i class="fe fe-bell"></i> <span>Bidding Rooms</span></a>
                            </li>
                             <li>
                                <a href="{{ route('comments.published.index') }}"><i class="fe fe-document"></i> <span>My Bids</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.unpublished.index', [$area]) }}"><i class="fe fe-tiled"></i> <span>Banked Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.published.index', [$area]) }}"><i class="fe fe-money"></i> <span>Selling Coins</span></a>
                            </li>
                             <li>
                                <a href="{{ route('listings.history.index', [$area]) }}"><i class="fe fe-file"></i> <span>All Transactions</span></a>
                            </li>


                            <li>
                                <a href="{{ route('profile') }}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
                            </li>
                                <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <i class="fe fe-logout"></i>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /Sidebar -->


                <!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

                    <!-- Page Header -->
                    <div class="page-header">
                        <div class="row">
                            <div class="col">
                                <h3 class="page-title">Change Password</h3>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item active">Change Password</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Header -->






                                <!-- Change Password Tab -->


                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="card-title">Change Password</h5>
                                            <div class="row">
                                                <div class="col-md-10 col-lg-6">
                                                    <form method="POST" action="{{ route('change.password') }}">
                        @csrf

                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Change Password Tab -->


                    </div>

                </div>
            </div>
            <!-- /Page Wrapper -->

@endsection
