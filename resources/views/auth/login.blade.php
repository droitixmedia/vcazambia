@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
    <div class="auth-layout-wrap">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4"><img src="/logo.png" alt=""></div>
                        <div class="flex-grow-1"></div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">Login</h1>
                                  <form method="POST" class="form-horizontal" action="{{ route('login') }}">
                                          @csrf
                                    <div class="form-group">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                        <div class="mt-3">
                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                                        </div>




                                        <div class="mt-4 text-center">
                                            <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                                        </div>
                                    </form>
                                    <div class="mt-3">
                                            <a
                                               href="{{route('register')}}" class="btn btn-primary btn-block waves-effect waves-light" type="submit">or Create Account</a>
                                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
