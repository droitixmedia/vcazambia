@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
    <div class="auth-layout-wrap" >
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4"><img src="/logo.png" alt=""></div>
                        <div class="flex-grow-1"></div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">CREATE  ACCOUNT</h1>
                                 <form action="{{ route('register') }}" method="POST" autocomplete="off">
                                    @csrf

                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" type="text" value="{{ old('name') }}" required autofocus  placeholder="First Name">
                                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                    </div>

                                       <div class="form-group">
                                        <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" type="text" value="{{ old('surname') }}" required autofocus  placeholder="Last Name">
                                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" type="text" value="{{ old('phone') }}" required autofocus  placeholder="Cell Number">
                                         @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                    </div>


                                 <div class="form-group">

                                        <select name="bank" class="form-control">
                                       
                                        <option value="AIRTEL MONEY">AIRTEL MONEY</option>
                                        <option value="MTN">MTN</option>
                                       
                                       


                                        </select>
                                        </div>


                                        <div class="form-group">
                                        <input class="form-control{{ $errors->has('account') ? ' is-invalid' : '' }}" name="account" type="text" value="{{ old('account') }}" required autofocus  placeholder="Mobile Money Number">
                                         @if ($errors->has('account'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('account') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Choose a password">


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again">
                                    </div>

                                        <div class="mt-4">
                                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Create Account</button>
                                        </div>




                                    </form>

                                    <div class="mt-3">
                                            <a
                                               href="{{route('login')}}" class="btn btn-primary btn-block waves-effect waves-light" type="submit">or Login</a>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
