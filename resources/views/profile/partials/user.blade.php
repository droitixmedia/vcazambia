<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ $user->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{$user->fullname}}</h5>
                                <p>{{$user->province}}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                        <ul>

                            <li class="active"><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                            <li><a href="{{ route('comments.published.index') }}"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="{{route('files.published.index')}}"><span class="flaticon-doc"></span> Manage CV uploads</a></li>
                            <li><a href="{{route('files.upload.index')}}"><span class="flaticon-upload"></span> Upload Documents</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>

                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                             <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Choose profile image and increase your chances of getting hired</h4>
                            </div>
                        <div class="my_profile_input form-group">
                     <form enctype="multipart/form-data" action="{{route('profile.update.avatar')}}" method="POST">
                                         <input  type="file" style="margin-left: 20px;" name="avatar">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class=" btn btn-sm btn-success"></form>
            </div>
        </div>
            <hr>
              <form action="{{route('profile.update')}}" method="post">
                    {{ csrf_field()  }}
                    <div class="my_profile_form_area">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">My Profile</h4>
                            </div>

                            <div class="col-lg-12">
                                <div class="my_profile_thumb_edit"></div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Full Name</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" value="{{ $user->fullname }}" name="fullname" placeholder="Martha Griffin">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                    <label  for="exampleFormControlInput3">Gender</label><br>
                                    <select name="gender" class="selectpicker">
                                        <option value="{{$user->gender}}">{{$user->gender}}</option>
                                        <option  value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput4">Nationality</label><br>
                                    <select name="nationality" class="selectpicker">
                                        <option value="{{$user->nationality}}">{{$user->nationality}}</option>
                                        <option  value="South African">South African</option>
                                        <option value="Non South African">Non South African</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleInputPhone">Contact Number</label>
                                    <input type="text" class="form-control" id="exampleInputPhone" aria-describedby="phoneNumber" name="phone" value="{{$user->phone}}" placeholder="i.e 0691246780">
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleInputPhone">Your Occupation Title</label>
                                    <input type="text" class="form-control" id="exampleInputPhone" aria-describedby="phoneNumber" name="occupationtitle" value="{{$user->occupationtitle}}" >
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="exampleFormControlInput1">Email address</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1"  name="email" value="{{$user->email}}" placeholder="craig@gmail.com">
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput3">Ethnicity</label><br>
                                    <select name="ethnicity" class="selectpicker">
                                        <option value="{{$user->ethnicity}}">{{$user->ethnicity}}</option>
                                        <option value="Not Specified">Not Specified</option>
                                        <option selected="selected" value="Black">Black</option>
                                        <option value="Coloured">Coloured</option>
                                        <option value="Asian">Asian</option>
                                        <option value="Indian">Indian</option>
                                        <option value="White">White</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Birth Year</label><br>
                        <select name="birthdate" id="position" class="selectpicker">
                             @include('profile.partials._birthyear')
                               </select>
                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput9">Select Current Province</label><br>
                                    <select name="province"  class="selectpicker">
                                        <option value="{{$user->province}}">{{$user->province}}</option>
                                        <option selected="selected" value="Gauteng">Gauteng</option>
                                        <option value="Limpopo">Limpopo</option>
                                        <option value="North West">North West</option>
                                        <option value="Mpumalanga">Mpumalanga</option>
                                        <option value="Free State">Free State</option>
                                        <option value="Western Cape">Western Cape</option>
                                        <option value="KwaZulu Natal">KwaZulu Natal</option>
                                        <option value="Eastern Cape">Eastern Cape</option>
                                        <option value="Nothern Cape">Nothern Cape</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername2">Full Address</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="address" value="{{$user->address}}" placeholder="i.e 23 Pheasant,Attlassvile,Boksburg">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Highest Education Level</label><br>
                                    <select name="educationlevel" class="selectpicker">
                                         <option value="{{$user->educationlevel}}">{{$user->educationlevel}}</option>
                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option selected="selected" value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername2">Languages Spoken</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="languages" value="{{$user->languages}}" placeholder="Zulu,English,Afrikaans">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="my_resume_textarea mt20">
                                     <div class="form-group">
                                        <label for="exampleFormControlTextarea1">About Me</label>
                                        <textarea name="aboutme" value="{{$user->aboutme}}" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$user->aboutme}}
                                        </textarea>
                                      </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h4 class="fz18 mb20">Social Network</h4>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername">Facebook</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="facebook" value="{{$user->facebook}}" placeholder="Your profile url i.e https://www.facebook.com/vodacom">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername2">Twitter</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="twitter" value="{{$user->twitter}}"  >
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername2">Linkedin</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="linkedin" value="{{$user->linkedin}}">
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="validationServerUsername2">Instagram</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="instagram" value="{{$user->instagram}}">
                                </div>
                            </div>
                            <input type="hidden" id="custId" name="truecompany" value="0">
                             <input type="hidden" id="custId" name="website" value="NA">
                             <input type="hidden" id="custId" name="companyname" value="NA">
                             <input type="hidden" id="custId" name="companycategory" value="NA">
                             <input type="hidden" id="custId" name="teamsize" value="NA">
                             <input type="hidden" id="custId" name="aboutcompany" value="NA">
                             <input type="hidden" id="custId" name="companytype" value="NA">
                             <input type="hidden" id="custId" name="established" value="NA">
                             <input type="hidden" id="custId" name="avatar" value="{{$user->avatar}}">
                            <input type="hidden" id="custId" name="idnumber" value="NA">

                             <input type="hidden" id="custId" name="occupation" value="NA">

                              <input type="hidden" id="custId" name="currentsalary" value="NA">
                              <input type="hidden" id="custId" name="expectedsalary" value="NA">
                            <input type="hidden" id="custId" name="experience" value="NA">




                            <div class="col-lg-4">
                                <div class="my_profile_input">
                                    <button class="btn btn-lg btn-thm" href="#">Save Changes</button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
