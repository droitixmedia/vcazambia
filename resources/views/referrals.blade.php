@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Referral Calculations</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

                 <div class="row">
                   <div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead class="thead-light">
                                                    <tr>

                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Bank</th>
                                                    <th>User Bids Total</th>
                                                    <th>Your 5%</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($referrals as $ref)
                                                <tr>
                                                    <td>

                                                           {{$ref->name}} {{$ref->surname}}
                                                    </td>
                                                    <td>{{$ref->email}}</td>
                                                    <td>{{$ref->phone}}</td>
                                                    <td>{{$ref->bank}}</td>
                                                    <td>0</td>
                                                    <td>0</td>

                                                </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                            {{ $referrals->links() }}
                                        </div>
                </div>
                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
