@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


@include('layouts.partials.sidebar')

 <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            <div class="main-content">
                <div class="breadcrumb">
                    <h1>Impersonate</h1>
                    <ul>
                    </ul>
                </div>
                <div class="separator-breadcrumb border-top"></div>

 <form action="{{ route('admin.impersonate') }}" method="POST">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>

                            <button type="submit" class="btn btn-thm">Impersonate</button>
                            {{ csrf_field() }}
                        </form>
                 </div>


                <div class="border-top mb-5"></div>

                </div><!-- end of main-content -->
            </div><!-- Footer Start -->


@endsection
