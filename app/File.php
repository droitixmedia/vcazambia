<?php

namespace openjobs;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $fillable = ['user_id','name', 'filename'];

  public function ownedByUser(User $user)
    {
        return $this->user->id === $user->id;
    }


 public function user()
    {
        return $this->belongsTo(User::class);
    }



}
