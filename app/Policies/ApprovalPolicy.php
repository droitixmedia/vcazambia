<?php

namespace openjobs\Policies;

use openjobs\{User, Approval};
use Illuminate\Auth\Access\HandlesAuthorization;

class ApprovalPolicy
{
    use HandlesAuthorization;



    public function destroy(User $user, Approval $approval)
    {
        return $this->touch($user, $approval);
    }
     public function touch(User $user, Approval $approval)
    {
        return $comment->ownedByUser($user);
    }


}
