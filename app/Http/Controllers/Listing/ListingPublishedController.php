<?php

namespace openjobs\Http\Controllers\Listing;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ListingPublishedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user=Auth::user();
        $listings = $request->user()->listings()->with(['area'])->isLive()->latestFirst()->paginate(10);

        return view('user.listings.published.index', compact('listings','user'));
    }

    /**
     *
     *
     * @param area this is a side effect of the setup of the routes
     * @param category the category from which we want to extract listings
     * @return json response contain the listing in the given category
     * note that the response is json with the title an address of each listing always
     */
    public function listingsInCategory($area, $category)
    {

        $listings = \openjobs\Listing::where('category_id', $category)
            ->isLive()
            ->limit(10)
            ->orderBy('updated_at', 'ASC')
            ->get(['title', 'address']);

        return response()->json(['success' => true, 'other_places' => $listings]);
    }


    public function destroy(Area $area, Listing $listing)
    {
        $this->authorize('destroy', $listing);

        $listing->delete();

        return back()->withSuccess('Listing was deleted.');
    }
}

