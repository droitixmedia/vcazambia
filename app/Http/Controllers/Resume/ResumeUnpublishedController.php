<?php

namespace openjobs\Http\Controllers\Resume;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ResumeUnpublishedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user = Auth::user();
        $resumes = $request->user()->resumes()->with(['area'])->isNotLive()->latestFirst()->paginate(10);

        return view('user.resumes.unpublished.index', compact('resumes','user'));
    }
}

