<?php

namespace openjobs\Http\Controllers;

use openjobs\User;
use Illuminate\Http\Request;
use openjobs\Http\Controllers\DataTable\DataTableController;

class UserController extends DataTableController
{

   public function index()
    {
        $users = \App\User::orderBy('updated_at')->paginate(10);
        return view('admin.users')->with(compact('users'));
    }

    public function builder()
    {
        return User::query();
    }

    public function getDisplayableColumns()
    {
        return [
            'id',
            'fullname',
            'email',
            'created_at'
        ];
    }

     public function getUpdatableColumns()
    {
        return [
            'fullnamename',
            'email',
            'created_at'
        ];
    }

      public function update($id, Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'email' => 'required|unique:users,email,' . $id . '|email',
            'created_at' => 'date'
        ]);

        $this->builder->find($id)->update($request->only($this->getUpdatableColumns()));
    }
}
