<?php

namespace openjobs\Http\Controllers\Admin;


use Illuminate\Http\Request;
use openjobs\Listing;
use openjobs\User;
use openjobs\Http\Controllers\Controller;

class ListingController extends Controller
{
  public function display()
    {
        $listings = Listing::with(['user', 'area'])->latestFirst()->paginate(300);
        return view('admin.listings.index')->with(compact('listings'));
    }

     public function destroyListing($id)
    {
        Listing::where('id', $id)->delete();
        session()->flash('message', 'Listing deleted!');
         notify()->success('listing Deleted');
        return redirect()->back();
    }
}
